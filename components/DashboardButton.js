import { useRouter } from 'next/router'
import Link from "next/link";

function DashboardButton() {
    const router = useRouter()

    function handleClick() {
        router.push('/dashboard')
    }

    return (
        <Link href={`/books`}>
        <button>Go to Dashboard</button>
        </Link>
    )
}

export default DashboardButton
