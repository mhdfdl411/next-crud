import { useState } from 'react';
import Router from 'next/router';
import Layout from '../../../components/layout';
import axios from "axios";

function BookCreate() {

    //state
    const [bookId, setBookId] = useState('');
    const [title, setTitle] = useState('');
    const [author, setAuthor] = useState('');
    const [publisher, setPublisher] = useState('');
    const [publishDate, setPublishDate] = useState('');
    const [bookThickness, setBookThickness] = useState('');

    //state validation
    const [validation, setValidation] = useState({});

    //method "storeBook"
    const storeBook = async (e) => {
        e.preventDefault();

        //define formData
        const formData = new FormData();

        //append data to "formData"
        formData.append('book_id', bookId);
        formData.append('title', title);
        formData.append('author', author);
        formData.append('publisher', publisher);
        formData.append('publish_date', publishDate);
        formData.append('book_thickness', bookThickness);

        //send data to server
        await axios.post(`${process.env.NEXT_PUBLIC_API_BACKEND}/api/books`, formData)
            .then(() => {

                //redirect
                Router.push('/books')

            })
            .catch((error) => {

                //assign validation on state
                setValidation(error.response.data);
            })

    };

    return (
        <Layout>
            <div className="container" style={{ marginTop: '80px' }}>
                <div className="row">
                    <div className="col-md-12">
                        <div className="card border-0 rounded shadow-sm">
                            <div className="card-body">
                                <form onSubmit={ storeBook }>

                                    <div className="form-group mb-3">
                                        <label className="form-label fw-bold">Book Id</label>
                                        <input className="form-control" type="text" value={bookId} onChange={(e) => setBookId(e.target.value)} placeholder="Masukkan Id Buku" />
                                    </div>
                                    {
                                        validation.bookId &&
                                        <div className="alert alert-danger">
                                            {validation.bookId}
                                        </div>
                                    }

                                    <div className="form-group mb-3">
                                        <label className="form-label fw-bold">TITLE</label>
                                        <input className="form-control" type="text" value={title} onChange={(e) => setTitle(e.target.value)} placeholder="Masukkan Title" />
                                    </div>
                                    {
                                        validation.title &&
                                        <div className="alert alert-danger">
                                            {validation.title}
                                        </div>
                                    }

                                    <div className="form-group mb-3">
                                        <label className="form-label fw-bold">AUTHOR</label>
                                        <textarea className="form-control" rows={3} value={author} onChange={(e) => setAuthor(e.target.value)} placeholder="Masukkan Nama Pengarang" />
                                    </div>
                                    {
                                        validation.author &&
                                        <div className="alert alert-danger">
                                            {validation.author}
                                        </div>
                                    }

                                    <div className="form-group mb-3">
                                        <label className="form-label fw-bold">PUBLISHER</label>
                                        <textarea className="form-control" rows={3} value={publisher} onChange={(e) => setPublisher(e.target.value)} placeholder="Masukkan Nama Penerbit" />
                                    </div>
                                    {
                                        validation.publisher &&
                                        <div className="alert alert-danger">
                                            {validation.publisher}
                                        </div>
                                    }

                                    <div className="form-group mb-3">
                                        <label className="form-label fw-bold">TANGGAL TERBIT</label>
                                        <textarea className="form-control" rows={3} value={publishDate} onChange={(e) => setPublishDate(e.target.value)} placeholder="Masukkan Tanggal Terbit" />
                                    </div>
                                    {
                                        validation.publishDate &&
                                        <div className="alert alert-danger">
                                            {validation.publishDate}
                                        </div>
                                    }

                                    <div className="form-group mb-3">
                                        <label className="form-label fw-bold">KETEBALAN BUKU</label>
                                        <textarea className="form-control" rows={3} value={bookThickness} onChange={(e) => setBookThickness(e.target.value)} placeholder="Masukkan Ketebalan Buku" />
                                    </div>
                                    {
                                        validation.bookThickness &&
                                        <div className="alert alert-danger">
                                            {validation.bookThickness}
                                        </div>
                                    }


                                    <button className="btn btn-primary border-0 shadow-sm" type="submit">
                                        SIMPAN
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Layout>
    );

}

export default BookCreate;
