import Layout from "../../components/layout";
import Link from 'next/link';
import axios from "axios";
import process from "next/dist/build/webpack/loaders/resolve-url-loader/lib/postcss";
import {useRouter} from "next/router";

export async function getServerSideProps() {

    //http request
    const req  = await axios.get(`${process.env.NEXT_PUBLIC_API_BACKEND}/api/books`)
    console.log("inires", req);
    const res  = await req.data.data
    console.log("inires2", res);
    return {
        props: {
            books: res // <-- assign response
        },
    }
}

function BookIndex(props) {

    //destruct
    const { books } = props;

    //router
    const router = useRouter();

    //refresh data
    const refreshData = () => {
        router.replace(router.asPath);
    }

    //function "deleteBook"
    const deleteBook = async (book_id) => {

        //sending
        await axios.delete(`${process.env.NEXT_PUBLIC_API_BACKEND}/api/books/${book_id}`);

        //refresh data
        refreshData();

    }

    return(
        <Layout>
            <div className="container" style={{ marginTop: '80px' }}>
                <div className="row">
                    <div className="col-md-12">
                        <div className="card border-0 shadow-sm rounded-3">
                            <div className="card-body">
                                <Link href="/books/create">
                                    <button className="btn btn-primary border-0 shadow-sm mb-3">TAMBAH</button>
                                </Link>
                                <table className="table table-bordered mb-0">
                                    <thead>
                                    <tr>
                                        <th scope="col">ID BUKU</th>
                                        <th scope="col">JUDUL</th>
                                        <th scope="col">PENGARANG</th>
                                        <th scope="col">PENERBIT</th>
                                        <th scope="col">TANGGAL TERBIT</th>
                                        <th scope="col">TEBAL BUKU</th>
                                        <th scope="col">STATUS</th>
                                        <th scope="col">PEMINJAM</th>
                                        <th scope="col">TANGGAL PINJAM</th>
                                        <th scope="col">TANGGAL KEMBALI</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    { books.map((book) => (
                                        <tr key={ book.book_id }>
                                            <td>{ book.book_id }</td>
                                            <td>{ book.title }</td>
                                            <td>{ book.author }</td>
                                            <td>{ book.publisher }</td>
                                            <td>{ book.publish_date }</td>
                                            <td>{ book.book_thickness }</td>
                                            <td>{ book.status }</td>
                                            <td>{ book.borrower }</td>
                                            <td>{ book.borrow_date }</td>
                                            <td>{ book.return_date }</td>
                                            <td className="text-center">
                                                <Link href={`/books/edit/${book.book_id}`}>
                                                    <button className="btn btn-sm btn-primary border-0 shadow-sm mb-3 me-3">EDIT</button>
                                                </Link>

                                                <Link href={`/books/borrow/${book.book_id}`}>
                                                    <button className="btn btn-sm btn-primary border-0 shadow-sm mb-3 me-3">EDIT DATA PINJAM</button>
                                                </Link>

                                                <button onClick={() => deleteBook(book.book_id)} className="btn btn-sm btn-danger border-0 shadow-sm mb-3">DELETE</button>
                                            </td>
                                        </tr>
                                    )) }
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Layout>
    )
}

export default BookIndex
