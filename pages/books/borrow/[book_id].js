//import hook useState
import { useState } from 'react';

//import router
import Router from 'next/router';

//import layout
import Layout from '../../../components/layout';

//import axios
import axios from "axios";

//fetch with "getServerSideProps"
export async function getServerSideProps({ params }) {

    //http request
    const req  = await axios.get(`${process.env.NEXT_PUBLIC_API_BACKEND}/api/books/${params.book_id}`)
    const res  = await req.data.data

    return {
        props: {
            book: res[0] // <-- assign response
        },
    }
}

function BookEdit(props) {

    //destruct
    const { book } = props;

    //state
    const [status, setStatus] = useState(book.status);
    const [borrower, setBorrower] = useState(book.borrower);
    const [borrowDate, setBorrowDate] = useState(book.borrow_date);
    const [returnDate, setReturnDate] = useState(book.return_date);

    const [show, setShow] = useState(false);

    //state validation
    const [validation, setValidation] = useState({});

    //method "updateBook"
    const updateBook = async (e) => {
        e.preventDefault();

        //define formData
        const formData = new FormData();

        //append data to "formData"
        formData.append('status', status);
        formData.append('borrower', borrower);
        formData.append('borrow_date', borrowDate);
        formData.append('return_date', returnDate);

        //send data to server
        axios({
            headers: {
                // Overwrite Axios's automatically set Content-Type
                'Content-Type': 'application/json'
            },
            method: 'put',
            url: `${process.env.NEXT_PUBLIC_API_BACKEND}/api/books/updateborrowstatus/${book.book_id}`,
            data: {
                status: status,
                borrower: borrower,
                borrow_date: borrowDate,
                return_date: returnDate
            }
        }).then(() => {

            //redirect
            Router.push('/books')

        })
            .catch((error) => {

                //assign validation on state
                setValidation(error.response.data);
            });
    };

    return (
        <Layout>
            <div className="container" style={{ marginTop: '80px' }}>
                <div className="row">
                    <div className="col-md-12">
                        <div className="card border-0 rounded shadow-sm">
                            <div className="card-body">
                                <form onSubmit={ updateBook }>

                                    <div className="form-group mb-3">
                                        <label className="form-label fw-bold">STATUS (*Note : 0 (Available) or 1 (Borrowed))</label>
                                        <input className="form-control" type="text" value={status} onChange={(e) => setStatus(e.target.value)} placeholder="Masukkan Status" />
                                    </div>
                                    {
                                        validation.status &&
                                        <div className="alert alert-danger">
                                            {validation.status}
                                        </div>
                                    }

                                    <div className="form-group mb-3">
                                        <label className="form-label fw-bold">PEMINJAM</label>
                                        <input className="form-control" type="text" value={borrower} onChange={(e) => setBorrower(e.target.value)} placeholder="Masukkan Nama Peminjam" />
                                    </div>
                                    {
                                        validation.borrowDate &&
                                        <div className="alert alert-danger">
                                            {validation.borrowDate}
                                        </div>
                                    }

                                    <div className="form-group mb-3">
                                        <label className="form-label fw-bold">TANGGAL PINJAM</label>
                                        <input className="form-control" type="text" value={borrowDate} onChange={(e) => setBorrowDate(e.target.value)} placeholder="Masukkan Tanggal Pinjam" />
                                    </div>
                                    {
                                        validation.borrowDate &&
                                        <div className="alert alert-danger">
                                            {validation.borrowDate}
                                        </div>
                                    }

                                    <div className="form-group mb-3">
                                        <label className="form-label fw-bold">TANGGAL KEMBALI</label>
                                        <textarea className="form-control" type="text" value={returnDate} onChange={(e) => setReturnDate(e.target.value)} placeholder="Masukkan Tanggal Kembali" />
                                    </div>
                                    {
                                        validation.returnDate &&
                                        <div className="alert alert-danger">
                                            {validation.returnDate}
                                        </div>
                                    }


                                    <button className="btn btn-primary border-0 shadow-sm" type="submit">
                                        UPDATE
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Layout>
    );

}

export default BookEdit
