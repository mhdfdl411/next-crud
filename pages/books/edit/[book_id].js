//import hook useState
import { useState } from 'react';

//import router
import Router from 'next/router';

//import layout
import Layout from '../../../components/layout';

//import axios
import axios from "axios";

//fetch with "getServerSideProps"
export async function getServerSideProps({ params }) {

    //http request
    console.log("iniidbuku", params.book_id)
    const req  = await axios.get(`${process.env.NEXT_PUBLIC_API_BACKEND}/api/books/${params.book_id}`)
    const res  = await req.data.data
    console.log("inidatabuku", res)
    return {
        props: {
            book: res[0] // <-- assign response
        },
    }
}

function BookEdit(props) {

    //destruct
    const { book } = props;

    //state
    const [bookId, setBookId] = useState(book.book_id);
    const [title, setTitle] = useState(book.title);
    const [author, setAuthor] = useState(book.author);
    const [publisher, setPublisher] = useState(book.publisher);
    const [publishDate, setPublishDate] = useState(book.publish_date);
    const [bookThickness, setBookThickness] = useState(book.book_thickness);

    //state validation
    const [validation, setValidation] = useState({});

    //method "updateBook"
    const updateBook = async (e) => {
        e.preventDefault();

        //define formData
        const formData = new FormData();

        //append data to "formData"
        formData.append('book_id', bookId);
        formData.append('title', title);
        formData.append('author', author);
        formData.append('publisher', publisher);
        formData.append('publish_date', publishDate);
        formData.append('book_thickness', bookThickness);
        formData.append('_method', 'PUT');

        //send data to server
        await axios.post(`${process.env.NEXT_PUBLIC_API_BACKEND}/api/books/${book.book_id}`, formData)
            .then(() => {

                //redirect
                Router.push('/books')

            })
            .catch((error) => {

                //assign validation on state
                setValidation(error.response.data);
            })

    };

    return (
        <Layout>
            <div className="container" style={{ marginTop: '80px' }}>
                <div className="row">
                    <div className="col-md-12">
                        <div className="card border-0 rounded shadow-sm">
                            <div className="card-body">
                                <form onSubmit={ updateBook }>
                                    <div className="form-group mb-3">
                                        <label className="form-label fw-bold">ID BUKU</label>
                                        <input className="form-control" type="text" value={bookId} onChange={(e) => setBookId(e.target.value)} placeholder="Masukkan Id Buku" />
                                    </div>
                                    {
                                        validation.bookId &&
                                        <div className="alert alert-danger">
                                            {validation.bookId}
                                        </div>
                                    }

                                    <div className="form-group mb-3">
                                        <label className="form-label fw-bold">TITLE</label>
                                        <input className="form-control" type="text" value={title} onChange={(e) => setTitle(e.target.value)} placeholder="Masukkan Title" />
                                    </div>
                                    {
                                        validation.title &&
                                        <div className="alert alert-danger">
                                            {validation.title}
                                        </div>
                                    }

                                    <div className="form-group mb-3">
                                        <label className="form-label fw-bold">AUTHOR</label>
                                        <input className="form-control" type="text" value={author} onChange={(e) => setAuthor(e.target.value)} placeholder="Masukkan Pengarang" />
                                    </div>
                                    {
                                        validation.author &&
                                        <div className="alert alert-danger">
                                            {validation.author}
                                        </div>
                                    }


                                    <div className="form-group mb-3">
                                        <label className="form-label fw-bold">PENERBIT</label>
                                        <textarea className="form-control" type="text" value={publisher} onChange={(e) => setPublisher(e.target.value)} placeholder="Masukkan Penerbit" />
                                    </div>
                                    {
                                        validation.publisher &&
                                        <div className="alert alert-danger">
                                            {validation.publisher}
                                        </div>
                                    }

                                    <div className="form-group mb-3">
                                        <label className="form-label fw-bold">Tanggal Terbit</label>
                                        <textarea className="form-control" type="text" value={publishDate} onChange={(e) => setPublishDate(e.target.value)} placeholder="Masukkan Tanggal Terbit" />
                                    </div>
                                    {
                                        validation.publishDate &&
                                        <div className="alert alert-danger">
                                            {validation.publishDate}
                                        </div>
                                    }

                                    <div className="form-group mb-3">
                                        <label className="form-label fw-bold">Tebal Buku</label>
                                        <textarea className="form-control" type="text" value={bookThickness} onChange={(e) => setBookThickness(e.target.value)} placeholder="Masukkan Tebal Buku" />
                                    </div>
                                    {
                                        validation.bookThickness &&
                                        <div className="alert alert-danger">
                                            {validation.bookThickness}
                                        </div>
                                    }

                                    <button className="btn btn-primary border-0 shadow-sm" type="submit">
                                        UPDATE
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Layout>
    );

}

export default BookEdit
