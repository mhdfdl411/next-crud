import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'
import {useState} from "react";
import DashboardButton from "../components/DashboardButton";

function HomePage() {
    return (
        <div className="container content position-relative text-center justify-content-center align-content-center align-items-center">
            <h1>Welcome to Book Management!</h1>
            <DashboardButton />
        </div>
    )
}

export default HomePage
